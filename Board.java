package uet.oop.bomberman;

import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import uet.oop.bomberman.entities.*;
import uet.oop.bomberman.entities.items.BombItem;
import uet.oop.bomberman.entities.items.FlameItem;
import uet.oop.bomberman.entities.items.SpeedItem;
import uet.oop.bomberman.entities.moveableEntities.Balloon;
import uet.oop.bomberman.entities.moveableEntities.Bomber;
import uet.oop.bomberman.entities.moveableEntities.Oneal;
import uet.oop.bomberman.entities.stillEntities.Brick;
import uet.oop.bomberman.entities.stillEntities.Grass;
import uet.oop.bomberman.entities.Portal;
import uet.oop.bomberman.entities.stillEntities.Wall;
import uet.oop.bomberman.graphics.Sprite;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static uet.oop.bomberman.utils.TerminalColor.log;

public class Board {
  public int width;
  public int height;
  private List<Entity> entities = new ArrayList<>();
  private List<Entity> stillObjects = new ArrayList<>();
  private Scene scene = null;

  public Board(Scene scene) {
    this.scene = scene;
    loadLevel(1);
  }

  public Scene getScene() {
    return scene;
  }

  public List<Entity> getEntities() {
    return entities;
  }

  public List<Entity> getStillObjects() {
    return stillObjects;
  }

  public Entity getStillObjectByBoard(int boardX, int boardY) {
    // Trả về StillObject ở trên cùng
    for (int i = stillObjects.size() - 1; i >= 0; --i) {
      Entity object = stillObjects.get(i);
      if (object.getBoardX() == boardX && object.getBoardY() == boardY) {
        return object;
      }
    }
    return null;
  }

  public Entity getStillObjectByCanvas(int x, int y) {
    // Trả về StillObject ở trên cùng
    for (int i = stillObjects.size() - 1; i >= 0; --i) {
      Entity object = stillObjects.get(i);
      if (object.getBoardX() == x / Sprite.SCALED_SIZE && object.getBoardY() == y / Sprite.SCALED_SIZE) {
        return object;
      }
    }
    return null;
  }

  public void loadLevel(int level) {
    Scanner scan = null;
    try {
      scan = new Scanner(new FileReader("./res/levels/Level"+ level + ".txt")).useDelimiter("\\A");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    StringBuilder sb = new StringBuilder();
    while(true) {
      assert scan != null;
      if (!scan.hasNext()) break;
      sb.append(scan.next());
    }
    scan.close();
    //System.out.println(sb.toString());

    String[] lines = sb.toString().split("\n");
    String[] line1 = lines[0].split(" ");
    int rowCount = Integer.parseInt(line1[1].trim());
    this.height = rowCount;
    int colCount = Integer.parseInt(line1[2].trim());
    this.width = colCount;
    Character[][] mapMatrix = new Character[rowCount][colCount];
    for(int i = 1; i<= rowCount; ++i) {
      for (int j = 0; j < colCount; ++j) {
        mapMatrix[i-1][j] = lines[i].charAt(j);
      }
    }
    //Create map from matrix
    for(int i = 0; i< rowCount; ++i) {
      for (int j = 0; j < colCount; ++j) {
        stillObjects.add(new Grass(j, i ,Sprite.grass.getFxImage()));
      }
    }
    for(int i = 0; i< rowCount; ++i) {
      for (int j = 0; j < colCount; ++j) {
        Entity object = null;
        char c = mapMatrix[i][j];
        switch (c) {
          case '#':
            object = new Wall(j, i, Sprite.wall.getFxImage());
            break;
          case '*':
            object = new Brick(j, i, Sprite.brick.getFxImage());
            break;
          case 'x':
            object = new Portal(j, i, Sprite.portal.getFxImage());
            stillObjects.add(object);
            object = new Brick(j, i, Sprite.brick.getFxImage());
            break;
          case 'p':
            entities.add(new Bomber(j, i, Sprite.player_right.getFxImage(), this));
            break;
          case '1':
            entities.add(new Balloon(j, i, Sprite.balloom_right1.getFxImage()));
            break;
          case '2':
            entities.add(new Oneal(j, i, Sprite.oneal_right1.getFxImage()));
            break;
          case 'b':
            object = new BombItem(j, i, Sprite.powerup_bombs.getFxImage());
            stillObjects.add(object);
            object = new Brick(j, i, Sprite.brick.getFxImage());
            break;
          case 'f':
            object = new FlameItem(j, i, Sprite.powerup_flames.getFxImage());
            stillObjects.add(object);
            object = new Brick(j, i, Sprite.brick.getFxImage());
            break;
          case 's':
            object = new SpeedItem(j, i, Sprite.powerup_speed.getFxImage());
            stillObjects.add(object);
            object = new Brick(j, i, Sprite.brick.getFxImage());
            break;
          default:
            break;
        }
        if(object !=null) stillObjects.add(object);
      }
      System.out.println();
    }
  }

  public void update() {
    entities.forEach(Entity::update);
    stillObjects.forEach(Entity::update);
  }

  public void render(GraphicsContext gc) {
    // Vẽ Entities sau khi vẽ stillObjects
    stillObjects.forEach(g -> g.render(gc));
    entities.forEach(g -> g.render(gc));
  }
}
